from flask import Flask, Response, request
import loader, requests

app = Flask(__name__)

@app.route('/time', methods=['POST'])
def get_time():
    keys = request.get_json()['keys']
    times = []
    if len(keys) < 2:
        return "Cannot resolve route: not enough keys"
    for i in range(0, len(keys) - 1):
        time = get_time_from_to(keys[i], keys[i+1])
        times.append(time)
    resp = Response(str(create_optimal_time(times)).replace("\'", "\""))
    resp.headers["Content-Type"] = "application/json"
    return resp



s = requests.Session()
def get_time_from_to(_from, _to):
    resp = loader.get_time(_from, _to, req=s)
    if "error" in resp:
        raise "No route"

    times = []
    for segment in resp["segments"]:
        time = {"departure": segment["departure"],"arrival": segment["arrival"]}
        times.append(time)

    return times

def create_optimal_time(times):
    result = []
    previous = "00:00:00"
    for i in range(0, len(times)):
        nearest = get_nearest_time(previous, times[i])
        previous = nearest["arrival"]
        result.append(nearest)
    return result

def get_nearest_time(previous, variants):
    for variant in variants:
            if get_number_by_time(variant["departure"]) > get_number_by_time(previous):
                return variant
    return variants[0]

def get_number_by_time(time):
    h, m, s = [int(s) for s in time.split(':')]
    return h*10000 + m*100 + s