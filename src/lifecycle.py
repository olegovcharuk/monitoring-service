import informator, sys

def handle_command(command):
    if command == 'nothing':
        print("No commands found.")
    elif command == 'fillds':
        print("Command: fill Data Service")
        informator.fill_data_service()
    elif command == 'fillrs':
        print("Command: fill Resolving Service")
        informator.fill_resolving_service()
    else:
        print("Command < {} > not found.".format(command))

if __name__ == '__main__':
    try:
        command = sys.argv[1]
    except IndexError:
        command = "nothing"
    handle_command(command)    



