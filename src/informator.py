import loader, converter
import requests

DATA_SERVICE_URL = "http://localhost:24530"
RESOLVING_SERVICE_URL = "http://localhost:24531"
headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}


def fill_data_service():
    print("Prepairing data...")
    stations, keys = loader.get_suburban_stations()
    converted_stations = converter.covert_stations_for_ds(stations)

    suburban_connections = loader.get_suburban_connections()
    # imaginary_connections = loader.get_auxiliary_connections()
    # connections = suburban_connections + imaginary_connections

    print("Sending data: {} stations".format(len(converted_stations)))
    req = DATA_SERVICE_URL + "/stations/add/all" 
    response = requests.post(req, headers=headers, json=converted_stations)
    response.raise_for_status()

    # print("Sending data: {} connections: {} real and {} imaginary."\
    #       .format(len(connections), len(suburban_connections), len(imaginary_connections)))

    print("Sending data: {} connections.".format(len(suburban_connections)))

    req = DATA_SERVICE_URL + "/connections/add/all"
    response = requests.post(req, headers=headers, json=suburban_connections)
    # response = requests.post(req, headers=headers, json=connections)
    response.raise_for_status()

    print("Complete!")


def fill_resolving_service():
    print("Fetching stations from Data Service...")
    req = DATA_SERVICE_URL + "/stations/all"
    response = requests.get(req, headers=headers)
    response.raise_for_status()
    stations = response.text
    print("Found {} stations".format(len(response.json())))

    print("Fetching connections from Data Service...")
    req = DATA_SERVICE_URL + "/connections/all"
    response = requests.get(req, headers=headers)
    response.raise_for_status()
    connections = response.text
    print("Found {} connections".format(len(response.json())))

    print("Sending data to Resolving Service...")
    req = RESOLVING_SERVICE_URL + "/graph/init"

    requests.post(req, headers=headers, json={'stations': stations, 'connections': connections})

    print("Complete!")
