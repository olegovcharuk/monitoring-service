def covert_stations_for_ds(stations):
    converted = []
    for station in stations:
        c_station = {}
        c_station["key"] = station["codes"]["yandex_code"]
        c_station["title"] = station["title"]
        c_station["direction"] = station["direction"]
        c_station["stationType"] = station["station_type"]
        c_station["latitude"] = station["latitude"]
        c_station["longitude"] = station["longitude"]
        c_station["country"] = station["country"]
        c_station["region"] = station["region"]
        c_station["settlement"] = station["settlement"]

        converted.append(c_station)
    return converted