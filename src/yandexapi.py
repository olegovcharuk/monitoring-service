import requests, json

API_KEY = "99bc48f7-1311-4b90-aec8-bccf73eff6c8"

get_all_stations_template = \
    "https://api.rasp.yandex.net/v3.0/stations_list/?apikey={}"
def get_all_stations(req=requests):
    request = get_all_stations_template.format(API_KEY)
    response = req.get(request)
    return response.text

get_routes_between_stations_template = \
    "https://api.rasp.yandex.net/v3.0/search/" + \
    "?apikey={}&from={}&to={}&transport_types=suburban" + \
    "&offset={}&transfers={}"
def get_routes_between_stations(_from, _to, offset="0", \
                        transfers="false", req=requests):
    request = get_routes_between_stations_template\
        .format(API_KEY, _from, _to, offset, transfers)
    response = req.get(request)
    return response.text

get_nearest_stations_template = \
    "https://api.rasp.yandex.net/v3.0/nearest_stations/" + \
    "?apikey={}&lat={}&lng={}&transport_types={}&distance={}&offset={}"
def get_nearest_stations(lat, lng, transport_types, \
            distance="50", offset="0", req=requests):
    request = get_nearest_stations_template\
        .format(API_KEY, lat, lng, transport_types, distance, offset)
    response = req.get(request)
    return response.text

get_station_timetable_template = \
    "https://api.rasp.yandex.net/v3.0/schedule/?apikey={}" + \
    "&station={}&transport_types=suburban"
def has_suburban_route (station, req=requests):
    request = get_station_timetable_template.format(API_KEY, station)
    response = req.get(request)
    json_response = json.loads(response.text)
    if "error" in json_response:
        return False
    return json_response["pagination"]["total"] > 0

