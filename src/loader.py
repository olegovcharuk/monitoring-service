import yandexapi as api
from filters import SuburbanFilter
import json, os, requests

cache_folder = os.path.dirname(os.path.abspath(__file__)) + "/../cache/{}"

def get_all_stations():
    file_path = cache_folder.format("all_stations.json")
    if os.path.isfile(file_path):
        with open(file_path, encoding='utf-8') as stations_file:
            stations_text = stations_file.read()
    else:
        stations_text = api.get_all_stations()
        with open (file_path, 'w') as output:
            output.write(stations_text)

    return json.loads(stations_text)


def_filter = SuburbanFilter()
def get_suburban_stations(station_filter=def_filter):
    all_stations = get_all_stations()

    fn = station_filter.name
    st_file_path = cache_folder.format(fn + "_stations.json")
    key_file_path = cache_folder.format(fn + "_st_keys.json")

    if os.path.isfile(st_file_path) and os.path.isfile(key_file_path):
        with open(st_file_path, encoding='utf-8') as filtered_stations:
            station_list = json.loads(filtered_stations.read())
        with open(key_file_path, encoding='utf-8') as keys:
            station_keys = json.loads(keys.read())
    else:
        i = 0
        s = requests.Session()
        station_list = []
        station_keys = []
        for country in all_stations["countries"]:
            if country["title"] in station_filter.target_countries:
                for region in country["regions"]:
                    for settlement in region["settlements"]:
                        for station in settlement["stations"]:
                            key = station["codes"]["yandex_code"]
                            if (station['transport_type'] in \
                                station_filter.target_transport_type \
                                and station['latitude'] != '' \
                            ):
                                if (api.has_suburban_route(key, s)):
                                    station["country"] = country["title"]
                                    station["region"] = region["title"]
                                    station["settlement"] = settlement["title"]
                                    station_list.append(station)
                                    station_keys.append(key)
                                    i+=1
                                    print(i)

        with open(st_file_path, 'w') as filtered_stations:
            filtered_stations.write(json.dumps(station_list))
        with open(key_file_path, 'w') as keys:
            keys.write(json.dumps(station_keys))

    return station_list, station_keys

def get_connections_with_nearests(station, nearests, \
                connections, req=requests.Session()):
    key_from = station["codes"]["yandex_code"]    
    for nearest in nearests:
        key_to = nearest["codes"]["yandex_code"]
        founded = api.get_routes_between_stations(\
            key_from, key_to, req=req)
        if "error"  in founded:
            continue
        if founded["pagination"]["total"] > 0:
            connections.append({"from": key_from,"to": key_to})



def get_suburban_connections():
    cn_file_path = cache_folder.format("suburban_connections.json")
    if os.path.isfile(cn_file_path):
        with open(cn_file_path, encoding='utf-8') as cn_file:
            connections = json.loads(cn_file.read())
    else:
        s = requests.Session()
        connections = []
        stations = get_suburban_stations()
        for station in stations:
            lat = station["latitude"]
            lng = station["longitude"]
            nearest_stations = json.loads(\
                api.get_nearest_stations(lat, lng, req=s))
            for nearest_station in nearest_stations:
                get_connections_with_nearests(station, nearest_station,\
                    connections, req=s)

    return connections

def get_auxiliary_connections():
    settlement_to_stations = {}
    auxiliary_connections = []
    stations, keys = get_suburban_stations()
    for station in stations:
        settlement = station["settlement"]
        if settlement == "":
            continue
        if settlement not in settlement_to_stations:
            settlement_to_stations[settlement] = []
        settlement_to_stations[settlement].append(station)
    for settlement, stations in settlement_to_stations.items():
        for i in range(0, len(stations) - 1):
            for j in range(i + 1, len(stations)):
                frm = stations[i]["codes"]["yandex_code"]
                to = stations[j]["codes"]["yandex_code"]
                im_connection = {"from": frm, "to": to, "im": "true"}
                auxiliary_connections.append(im_connection)

    return auxiliary_connections

if __name__ == '__main__':
    get_auxiliary_connections()



def get_time(_from, _to, req=requests):
    return json.loads(api.get_routes_between_stations(_from, _to, req=req))

if __name__ == "__main__":
    get_suburban_stations()
